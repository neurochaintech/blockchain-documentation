"use strict";

import fs from "fs";

export default class FileSystemTools {

    static doesExist({ inputPath }) {
        let exist = false;
        
        try {
            exist = fs.existsSync(inputPath);
        } catch(err) {
            console.error(err)
        }
    
        return exist;
    }

    static saveToFile({ inputPath, data }) {
        fs.writeFileSync(inputPath, data, function(err) {
            if (err) {
                console.log(err);
            }
        });
    }
    
    static appendToFile({ inputPath, data }) {
        fs.appendFileSync(inputPath, data, function(err) {
            if (err) {
                console.log(err);
            }
        });
    }

    static readFile({ inputPath }) {
        let content = "";
    
        if( this.doesExist({ inputPath: inputPath }) ) {
            try {
                const rawOutput = fs.readFileSync(inputPath);
                content = rawOutput.toString();
            } catch(err) {
                console.error(err)
            }
        }

        return content
    }
    
    static deleteFile({ inputPath }) {
        if(this.doesExist({ inputPath: inputPath })) {
            try {
                fs.unlinkSync(inputPath);
            } catch(error) {
                console.error(error);
            }
        }
    }

    static getFilesPaths({ inputPath, recursive = true, whitelist = ["*"], output = []  }) {
        if ( this.doesExist({ inputPath: inputPath }) ) {
            const files = fs.readdirSync(inputPath);

            for( const file of files ) {
                const path = `${inputPath}/${file}`;

                if( fs.lstatSync(path).isDirectory() ) {
                    this.getFilesPaths({ inputPath: path, recursive: recursive, whitelist: whitelist, output: output} );

                } else {
                    if( whitelist[0] === "*" ) {
                        output.push(path);
                    } else {
                        
                        let add = false;
                        for( const whiteKey of whitelist ) {
                            add |= file.includes(whiteKey);
                        }
                        if(add) {
                            output.push(path);
                        }

                    }
                }
            }
        }

        return output;
    }
    
    static createDirectory({ inputPath }) {
        if( !this.doesExist({ inputPath: inputPath }) ) {
            fs.mkdirSync(inputPath, { recursive: true });
        }
    }

    
    static deleteDirectory({ inputPath }) {
        if( this.doesExist({ inputPath: inputPath }) ) {
            fs.rmSync(inputPath, { recursive: true, force: true });
        }
    }
}