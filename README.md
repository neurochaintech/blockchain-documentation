# Blockchain Documentation

Documentation to help newcomers or to write presentations.

## Generate PDFs

<ol>
    <li>Put .md in "/mds"</li>
    <li>npm install</li>
    <li>npm run generateDocumentation</li>
    <li>Outputs are in "/pdfs"</li>
</ol>