"use strict";

import mdpdf from 'mdpdf';

import FileSystemTools from "./tools/fileSystem_tools.js";

async function mdToPdf () {
    const documentationPath = `${process.cwd()}/mds`;
    const mdsPaths = FileSystemTools.getFilesPaths({ inputPath: documentationPath, recursive: false, whitelist: [".md"]  });
    for(const mdPath of mdsPaths) {
        const pathElements = mdPath.split('/');
        const file = pathElements[pathElements.length - 1];
        const fileName = file.replace('.md','');

        const outputFilePath = `${process.cwd()}/pdfs/${fileName}.pdf`;

        const options = {
            source: mdPath,
            destination: outputFilePath,
	        // header: `${process.cwd()}/mds/header.html`,
            // footer: `${process.cwd()}/mds/footer.html`,
            pdf: {
                format: 'A4',
                orientation: 'portrait',
                quality: '100',
                header: {
                    height: '20mm'
                },
                footer: {
                    height: '20mm'
                },
                border: {
                    top: '20mm',
                    left: '10mm',
                    bottom: '20mm',
                    right: '10mm'
                }
            }
        };
        await mdpdf.convert(options);
        console.log('PDF generated:', outputFilePath);
    }
}

async function main() {
    try {
        await mdToPdf();        
    } catch(e) {
        console.error(e);
        process.exit(1);
    }
};
main();