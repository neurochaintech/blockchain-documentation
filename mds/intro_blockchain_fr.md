<style>
p {
    text-align: justify;
}

img {
    width: 100%;
}

.figure {
    text-align: center;
}

.figure p{
    text-decoration: underline;
    text-align: center;
}

.info {
    color: #055160;
    background-color: #cff4fc;
    border-color: #b6effb;
    padding: 1rem 1rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: .25rem;
}
</style>


<h1>Blockchain</h1>

<h2>Qu'est-ce que c'est?</h2>
<p>
    La blockchain est avant tout une <b>technologie de stockage et de transmission d'informations</b>. Cette technologie offre des <b>normes élevées de transparence et de sécurité</b> car elle fonctionne sans organe central de contrôle.
</p>
<p>
    Plus précisément, la blockchain est <b>un registre, une grande base de données, un grand livre distribué</b>. 
    La blockchain a la particularité d'être partagée simultanément avec tous ses utilisateurs, qui sont tous également titulaires de ce même registre.
    De plus, il contient l'historique de tous les échanges entre ses utilisateurs depuis sa création.
</p>
<p>
    <b>Tous ces utilisateurs ont la capacité d'y saisir des données, selon des règles précises fixées par un protocole informatique très sécurisé grâce à la cryptographie.</b>
</p>

<h2>Pourquoi a-t-elle été inventée ?</h2>
<p>
    En 2008, le (supposé) créateur Satoshi Nakamoto pensait que notre confiance dans les organisations traditionnelles pour conserver les archives (comme les banques) était mal placée. Il voulait créer un <b>système indépendant</b> pour que nous n'ayons plus besoin de tiers. Avec cette logique, la technologie blockchain a été créée.
</p>

<div class="figure">
    <img src="../imgs/1_transactionClassique.png" />
    <p>Avec des tiers</p>
</div>

<div class="figure">
    <img src="../imgs/1_transactionBlockchain.png" />
    <p>Avec la blockchain</p>
</div>

<h2>Comment fonctionne la blockchain ?</h2>

Supposons qu'Alice envoie une transaction à Bob. Voici les principales étapes :
<ol>
    <li>Alice définit sa transaction : elle choisit ce qu'elle veut envoyer à Bob.</li>
    <li>La transaction est formatée, <b>chiffrée et authentifiée</b> : Alice et Bob sont représentés par des clefs qui
    les identifient et donnent accès à leur "compte" ou "portefeuille" de valeur sur le système. Les données sont chiffrées.</li>
    <li>La transaction est envoyée au réseau puis stockée.</li>
    <li>Le <b>réseau</b> est composé d'ordinateurs reliés entre eux, on les appelle les <b>"nœuds"</b>.</li>
    <li>Lorsque suffisamment de transactions sont stockées, ou qu'un certain temps s'est écoulé, elles sont conditionnées dans un pré-bloc. Le seuil dépend de la politique de blockchain.</li>
    <li>Ce <b>pré-bloc doit être forgé</b>, approuvé et autorisé avant d'être ajouté à la chaîne.</li>
    <li>Pour ce faire, les nœuds de <b>minage</b> tentent de <b>résoudre un problème mathématique</b> qui nécessite une certaine quantité de
    Ressources. <b>Il ne peut être résolu que par de nombreux essais et erreurs</b>. Les chances de résoudre le problème sont autour
    1 sur 5,9 billions au mieux.</li>
    <li>Lorsque la solution est trouvée, elle est ajoutée au bloc. Le bloc est enfin forgé. <b>Le premier mineur qui
    trouve la solution propage le bloc</b>.</li>
    <li><b>Avant d'ajouter le bloc à la blockchain, la majorité des nœuds doivent valider le bloc</b> :
    ils vérifient si la solution du bloc est correcte, s'ils trouvent les mêmes résultats avec des éléments bruts du bloc.</li>
    <li>Lorsque la <b>majorité a approuvé le bloc</b>, il est ajouté à la blockchain existante.</li>
    <li>Après cette phase de validation, <b>le nœud qui a forgé le bloc est récompensé pour sa "preuve de travail" avec de la 
    crypto-monnaie</b> car le processus de déchiffrement demande beaucoup de calculs et de ressources (beaucoup d'essais).</li>
    <li>La <b>blockchain mise à jour est distribuée sur le réseau</b> afin que chacun d'eux ait une copie du
    grand livre distribué réel.</li>
    <li>La transaction est terminée : Bob reçoit ce qu'Alice lui a envoyé.</li>
</ol>

<div class="figure">
    <img src="../imgs/1_mainSteps.png" />
    <p>Les principales étapes</p>
</div>

<h2>Quels sont les avantages ?</h2>
<ul>
    <li>La <b>rapidité des transactions</b> : la validation d'un bloc ne prend que quelques secondes à quelques minutes car le réseau est sollicité.</li>
    <li>La <b>sécurité du système</b> : la validation est effectuée par un ensemble d'utilisateurs différents, qui ne se connaissent pas
    l'un l'autre. Cela permet de se prémunir contre les risques de malveillance ou de piratage, puisque les nœuds valident,
    dupliquer et vérifier la blockchain en continu.</li>
    <li>La <b>productivité et l'efficacité</b> : la blockchain confie l'organisation des échanges à un ordinateur
    protocole, qui réduit mécaniquement les coûts de transaction ou de centralisation existant dans les
    systèmes (coûts financiers, coûts de contrôle ou de certification, recours à des intermédiaires rémunérés pour
    leurs services, automatisation de certains services, etc.).</li>
</ul>

<h2>Quelles sont les applications ?</h2>

<h3>Quelques exemples</h3>
<p>
    La blockchain représente une innovation majeure qui est notamment utilisée dans le secteur bancaire. En effet,
    historiquement, la technologie blockchain s'est développée pour supporter les transactions réalisées via les crypto-monnaies /
    les crypto-actifs (dont les bitcoins qui en sont la forme la plus connue) et qui ont pour principale
    caractéristique de ne pas dépendre de l'organisation. Centraliser (comme une banque centrale) et être international. Cependant, cela ne se limite pas aux crypto-monnaies. De nombreux domaines et industries, commerciaux et non commerciaux,
    publics ou privés, utilisent déjà la blockchain ou envisagent de le faire dans les années à venir.
</p>

<h3>Le secteur bancaire</h3>
<p>
    Dans le secteur bancaire, la technologie ouvre la possibilité de <b>valider les transactions sans tiers
    parties</b>, ce qui devrait <b>permettre de certifier les transactions dans des délais beaucoup plus courts</b>. La blockchain peut aussi
    favoriser le <b>partage d'informations entre acteurs concurrents d'une place financière dans le respect
    le secret de leurs données commerciales</b> et, ce faisant, faciliter la gestion de structures communes ou
    la gestion d'instruments en réduisant les coûts de contrats et les frais d'administration.
</p>

<h3>Le secteur de l'assurance</h3>
<p>
    Dans le secteur de l'assurance, l'apport de la blockchain se situe dans <b>l'automatisation des remboursements
    procédures et la réduction de certaines formalités</b> pour les entreprises et leurs clients, à condition que
    les hypothèses et les conditions d'indemnisation et de dommages soient clairement établies.
</p>

<h3>Le secteur logistique</h3>
<p>
    Dans le secteur de la logistique, la blockchain présente deux avantages : <b>assurer la traçabilité des produits</b>, ainsi que <b>tracer
    des différentes interventions dans une chaîne de production et de distribution</b>. Ainsi, cela permet d'alléger
    les formalités et créer les conditions d'une coopération entre les acteurs d'une filière, notamment en termes
    de l'échange d'informations. Cette utilisation pourrait également trouver une application dans le secteur agro-alimentaire pour la
    traçabilité des aliments, ce serait particulièrement intéressant en cas de crise sanitaire...
</p>

</p>
    Comme vous pouvez le constater, de nombreux secteurs sont potentiellement concernés par l'utilisation de la technologie blockchain : santé, immobilier, luxe, aéronautique, etc.
</p>

<div class="info">
    <h2>Résumons!</h2>
    <hr/>
    <p>
        La blockchain :
        <ul>
            <li>est une technologie de stockage et de transmission de données, prenant la forme d'une base de données;</li>
            <li>a la particularité d'être partagé simultanément avec tous ses utilisateurs et ne dépend d'aucun corps central;</li>
            <li>a l'avantage d'être rapide et sécurisé;</li>
            <li>a un périmètre bien plus large que les crypto-monnaies/crypto-actifs (assurance, logistique, énergie, industrie, santé, etc.) uniquement.</li>
        </ul>
    </p>
    <hr/>
    <p>
        Les mots-clés : données numériques, technologie de stockage, persistante, infalsifiable, distribuée
    </p>
</div>