<style>
p {
    text-align: justify;
}

img {
    width: 100%;
}

.figure {
    text-align: center;
}

.figure p{
    text-decoration: underline;
    text-align: center;
}

.info {
    color: #055160;
    background-color: #cff4fc;
    border-color: #b6effb;
    padding: 1rem 1rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: .25rem;
}
</style>


<h1>Blockchain</h1>

<h2>What is it ?</h2>
<p>
    Blockchain is primarily a <b>technology for storing and transmitting information</b>. This technology offers <b>high standards of transparency and security</b> because it operates without a central control body.
</p>
<p>
    To be more precise, blockchain is a <b>register, a large database, a distributed ledger</b>.
    It has the particularity of being <b>shared simultaneously with all of its users, 
    all of whom are also holders of this register</b>. 
    Moreover, it contains <b>the history of all exchanges between its users since its creation</b>.
</p>
<p>
    <b>All these users have the capacity to enter data into it, according to specific rules fixed by a very well-secured computer protocol thanks to cryptography</b>. 
</p>

<h2>Why was it invented ?</h2>
<p>
    In 2008, the (supposed) creator Satoshi Nakamoto thought our trust in traditional organisations to maintain records (like banks) was misplaced. He wanted to create an <b>independent system</b> so that we don't need third parties any more. With this logic, the blockchain technology was created. 
</p>

<div class="figure">
    <img src="../imgs/1_transactionClassique.png" />
    <p>With third parties</p>
</div>

<div class="figure">
    <img src="../imgs/1_transactionBlockchain.png" />
    <p>With blockchain</p>
</div>

<h2>How does the blockchain work ?</h2>

Let's suppose Alice send a transaction to Bob. Here are the main steps:
<ol>
    <li>Alice sets her transaction: she chooses what she wants to send to Bob.</li>
    <li>The transaction is standardised, <b>encrypted and authenticated</b>: Alice and Bob are represented by keys that identify them and gives access to their "account" or "wallet" of value on the system. Data are ciphered.</li>
    <li>The transaction is sent to the network and stored.</li>
    <li>The <b>network</b> is composed of computers linked to each other, they are called <b>the "nodes"</b>.</li>
    <li>When enough transactions are stored, or that a certain time has passed, they are packaged in a pre-block. The threshold depends on the blockchain policy.</li>
    <li>This <b>pre-block needs to be forged</b>, approved, and authorised, before it is added to the chain.</li>
    <li>To do so, <b>mining</b> nodes try to <b>resolve a mathematical problem</b> that requires a certain amount of resources. It can be solved only by many trials and errors. The odds of solving the problem are around 1 in 5.9 trillion at best.</li>
    <li>When the solution is found, it is added to the block. The block is finally forged. The <b>first miner who found the solution spread the block</b>.</li>
    <li><b>Before adding the block to the blockchain, the majority of nodes must validate the block</b>: they verify if the block's solution is correct, if they find the same results with raw block elements.</li>
    <li>When the majority have approved the block, it is added to the existing blockchain.</li>
    <li>After this validation phase, <b>the node that forged the block is rewarded for its "proof of work" with cryptocurrency</b> as the decipher process asks a lot of calculus and resources (a lot of tries).</li>
    <li>The <b>updated blockchain is distributed across the network</b> so that each of them has got a copy of the actual distributed ledger.</li>
    <li>The transaction is complete: Bob receives what Alice sent to him.</li>
</ol>

<div class="figure">
    <img src="../imgs/1_mainSteps.png" />
    <p>The main steps</p>
</div>

<h2>What are the advantages ?</h2>
<ul>
    <li>The <b>speed of transactions</b>: the validation of a block only takes a few seconds to a few minutes as the network is solicited.</li>
    <li>The <b>security of the system</b>: the validation is carried out by a set of different users, who do not know each other. This helps guard against the risk of malicious or hijacking, since the nodes validate, duplicate and check the blockchain continuously.</li>
    <li>The <b>productivity and efficiency</b>: the blockchain entrusts the organisation of exchanges to a computer protocol, which mechanically reduces the transaction or centralisation costs existing in traditional systems (financial costs, control costs or certification, use of intermediaries who are remunerated for their service, automation of certain services, etc.).</li>
</ul>

<h2>What are the applications ?</h2>

<h3>Some examples</h3>
<p>
    The blockchain represents a major innovation which is used in particular in the banking sector. Indeed, historically, blockchain technology has developed to support transactions carried out via cryptocurrencies / crypto-assets (including bitcoins which are the most well-known form) and which have the main characteristic of not depending on the organisation. Centralising (like a central bank) and being international. However, it is not limited to cryptocurrencies. Many fields and industries, commercial and non-commercial, public or private, are already using blockchain or planning to do so in the coming years.
</p>

<h3>Banking sector</h3>
<p>
    In the banking sector, technology opens up the possibility of <b>validating transactions without any third parties</b>, which should <b>allow transactions to be certified in much shorter timeframes</b>. Blockchain can also promote the <b>sharing of information between players competing with a financial centre while respecting the secrecy of their commercial data</b> and, in doing so, facilitate the management of common structures or instruments by reducing contract costs and administration fees.
</p>

<h3>The insurance sector</h3>
<p>
    In the insurance sector, the contribution of the blockchain is in the <b>automation of reimbursement procedures and the reduction of certain formalities</b> for companies and their clients, provided that the assumptions and compensation and damage conditions are clearly established.
</p>

<h3>Logistic sector</h3>
<p>
    In the logistic sector, blockchain has two advantages: <b>ensuring product traceability</b>, as well as <b>the memory of the various interventions in a production and distribution chain</b>. Thus, this makes it possible to lighten the formalities and create the conditions for cooperation between the players in a sector, particularly in terms of the exchange of information. This use could also find an application in the agro-food sector for the traceability of food, it would be particularly interesting in the event of a health crisis...
</p>

</p>
    As you can see, many sectors are potentially affected by the use of blockchain technology: healthcare, real estate, luxury goods, aeronautics, etc.
</p>

<div class="info">
    <h2>Let's sum up!</h2>
    <hr/>
    <p>
        The blockchain:
        <ul>
            <li>is a technology for storing and transmitting data, taking the form of a database;</li>
            <li>has the particularity of being shared simultaneously with all its users and does not depend on any central body;</li>
            <li>has the advantage of being fast and secure;</li>
            <li>has a scope much wider than cryptocurrencies / crypto-assets (insurance, logistics, energy, industry, health, etc.) only.</li>
        </ul>
    </p>
    <hr/>
    <p>
    Keywords: digital data, storage technology, persistent, tamper-proof, distributed
    </p>
</div>